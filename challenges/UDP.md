# Client/Server communication using UDP sockets

### Problem description

Developing a transmission protocol, such as Bolina, means that sometimes you have to debug the quality of the network link. And we know that two of the variables that can significant decrease the performance of a network transfer are latency and packet loss.

The goal of this challenge is to create two separate processes (a Client and a Server) that will communicate through UDP to detect the existent packet loss for a given transfer rate.

How would you implement the communication?

### Requirements

* The Server will send randomly generated data accordingly to the Client's configuration.
* The Client configuration consists in:
    * transfer time (in seconds)
    * data generation rate (number of bits to be transferred per second, in Kbits/sec or Mbits/sec)
* Client and Server should print the reception/transmission rate at each second
* Client should print the total packet loss at the end of the transfer
* The Server may handle multiple sequential client transfers, but only handles one client at a time 


### Bonus Points

* Calculate the latency between Client and Server
 

### Input/Output Example

    server$
        ./server
            > waiting for client
            > new client connected
            > 0.0- 1.0 sec  sent 18751600 bytes at 150 Mbits/sec
            > 1.0- 2.0 sec  sent 18751600 bytes at 150 Mbits/sec
            > 2.0- 3.0 sec  sent 18751600 bytes at 150 Mbits/sec
            > finished
            > 0.0- 3.0 sec  sent 562548000 bytes at 150 Mbits/sec
            >
            > waiting for client
        
    client$
        ./client 192.168.1.1 -t 3 -r 150
            > waiting for packets from to 192.168.1.1
            > 0.0- 1.0 sec  received 18751600 bytes at 150 Mbits/sec
            > 1.0- 2.0 sec  received 18751600 bytes at 150 Mbits/sec
            > 2.0- 3.0 sec  received 18751600 bytes at 150 Mbits/sec
            > finished
            > 0.0- 3.0 sec  received 562548000 bytes out of 562548000 at 150 Mbits/sec, with 0% packet loss and 25ms of latency
            
